﻿using UnityEngine;
using System.Collections;

public class BellScript : MonoBehaviour {
	private StateScript state;

	void Start() {
		state = StateScript.Instance();
	}

	void OnMouseDown() {
		float d = Vector3.Distance(GameObject.Find("BillMurray").transform.position, transform.parent.position);
		if (d > 1.3) {
			return;
		}

		GetComponent<AudioSource>().Play();
		if (state.bellRang)
			return;

		GameObject.Find("LobbyManDressed").GetComponentInChildren<BellboyScript>().StartRise();
		state.bellRang = true;

		state.AddToScore (5);
		GameObject guiObject = GameObject.Find ("GUI");
		if (guiObject) {
			GUIScript guiScript = guiObject.GetComponent<GUIScript> ();
			guiScript.DisplayMessage ("Ok a little creepy...");
		}
	}
}
