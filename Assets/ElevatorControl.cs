﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ElevatorControl : MonoBehaviour {
	//private StateScript state;
	private Animator animator;

	// Use this for initialization
	void Start () {
		//state = StateScript.Instance();
		animator = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	// The elevator button has been pushed, random wait until it should open
	public void CallElevator()
	{
		Invoke ("OpenElevator", Random.Range (2, 5));

		// start it rising
		GameObject.Find("ElevatorCar2").GetComponentInChildren<ElevatorCar>().StartRise();
	}

	// Open the elevator doors and go ding!
	void OpenElevator()
	{
		AudioSource music = GetComponent<AudioSource> ();
		music.Play ();
		animator.SetBool ("Open", true);
	}

	public void CloseElevator()
	{
		Debug.Log ("close door");
		animator.SetBool ("Open", false);
	}
}
