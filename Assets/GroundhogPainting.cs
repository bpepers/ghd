﻿using UnityEngine;
using System.Collections;

public class GroundhogPainting : MonoBehaviour {
	void OnMouseDown() {
		float d = Vector3.Distance(GameObject.Find("BillMurray").transform.position, transform.position);
		Debug.Log("Dist = " + d);
		if (d > 1.8f) {
			return;
		}
		GameObject guiObject = GameObject.Find ("GUI");
		if (guiObject) {
			GUIScript guiScript = guiObject.GetComponent<GUIScript>();
			guiScript.DisplayMessage("I hate groundhogs.");
		}
	}
}
