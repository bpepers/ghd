﻿using UnityEngine;
using System.Collections;

public class ElevatorDownstairs : MonoBehaviour {
	//private StateScript state;
	private Animator animator;
	private bool open;
	private bool closing;
	private bool opening;

	// Use this for initialization
	void Start () {
		//state = StateScript.Instance();
		animator = GetComponent<Animator>();
		open = true;
		closing = false;
		opening = false;
	}
		
	void OnTriggerEnter(Collider other)
	{
		CloseElevator();
		closing = true;
	}

	// Open the elevator doors and go ding!
	void CloseElevator()
	{
		AudioSource music = GetComponent<AudioSource> ();
		music.Play ();
		animator.SetBool ("Close", true);
		Invoke("DoorsClosing", 0.7f);
		GetComponent<BoxCollider>().enabled = false;
	}

	void DoorsClosing() {
		GameObject.Find("ElevatorDoor0").GetComponent<BoxCollider>().enabled = true;
		Invoke("DoorsClosed", 0.5f);
	}

	void DoorsClosed() {
		open = false;
		closing = false;
		Debug.Log("close");
		GameObject.Find("ElevatorDoor0").GetComponent<BoxCollider>().enabled = false;
		Invoke("OpenDoors", 0.5f);
	}

	void OpenDoors() {
		opening = true;
		animator.SetBool ("Close", false);
		Invoke("FinishOpen", 0.5f);
	}

	void FinishOpen() {
		open = true;
		GameObject.Find("ElevatorDoor0").GetComponent<BoxCollider>().enabled = false;
	}
}
