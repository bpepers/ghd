﻿using UnityEngine;
using System.Collections;

public class WomanLoveMoves : MonoBehaviour {
	private bool moving = false;
	private float finalX = 0.1f;
	private bool running = false;

	// Use this for initialization
	void Start () {
		Invoke ("StartMoving", 2);
	}

	void StartMoving() {
		moving = true;
	}

	public void RunAway() {
		running = true;
		moving = false;
	}

	// Update is called once per frame
	void FixedUpdate () {
		if (running) {
			Vector3 v = transform.position;
			v.x += 0.1f;
			transform.position = v;
		}
		if (moving && transform.position.x <= finalX) {
			moving = false;
			Vector3 v = transform.position;
			v.x = finalX;
			transform.position = v;
		} else if (moving) {
			Vector3 v = transform.position;
			if (v.x >= 1.0f && v.x <= 1.2f)
				v.x -= 0.001f;
			else
				v.x -= 0.1f;
			transform.position = v;
		}
	}
}
