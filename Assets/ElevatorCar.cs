﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ElevatorCar : MonoBehaviour {
	private StateScript state;
	private float finalY = -0.1f;
	private bool rising = false;
	private BoxCollider floorCollider;

	void Start() {
		state = StateScript.Instance ();
		BoxCollider[] colliders = transform.GetComponents<BoxCollider> ();
		SetEnabled (colliders, false);
	}

	void SetEnabled(BoxCollider[] colliders, bool flag)
	{
		foreach (var collider in colliders) {
			collider.enabled = flag;
		}
	}

	public void StartRise() {
		rising = true;
	}

	void FixedUpdate() {
		if (rising && transform.parent.position.y >= finalY) {
			rising = false;
			Vector3 v = transform.parent.position;
			v.y = finalY;
			transform.parent.position = v;

			BoxCollider[] colliders = transform.GetComponents<BoxCollider> ();
			SetEnabled (colliders, true);
		} else if (rising) {
			Vector3 v = transform.parent.position;
			v.y += 0.1f;
			transform.parent.position = v;
		}
	}

	void OnTriggerEnter(Collider other) {
		ElevatorControl script = GameObject.Find ("Elevator").GetComponent<ElevatorControl> ();
		script.CloseElevator ();

		state.AddToScore (10);
		GameObject guiObject = GameObject.Find ("GUI");
		if (guiObject) {
			GUIScript guiScript = guiObject.GetComponent<GUIScript> ();
			guiScript.DisplayMessage ("Elevators are tricky!  Good job properly entering this one.");
		}

		Invoke ("GotoLobby", 2);
	}

	void GotoLobby() {
		SceneManager.LoadScene("LobbyScene");
	}
}
