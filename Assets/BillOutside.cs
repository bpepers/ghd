﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class BillOutside : MonoBehaviour {
	private StateScript state;
	GameObject undressedBill;
	GameObject dressedBill;
	GameObject undressedCatch;
	GameObject dressedCatch;

	// Use this for initialization
	void Start () {
		state = StateScript.Instance ();

		undressedBill = GameObject.Find("BillMurray").transform.GetChild(0).gameObject;
		dressedBill = GameObject.Find("BillMurray").transform.GetChild(1).gameObject;
		undressedCatch = GameObject.Find("BillMurray").transform.GetChild(2).gameObject;
		dressedCatch = GameObject.Find("BillMurray").transform.GetChild(3).gameObject;
		undressedCatch.SetActive(false);
		dressedCatch.SetActive(false);
		if (state.dressed) {
			Debug.Log ("he's dressed");
			dressedBill.SetActive (true);
			undressedBill.SetActive (false);
		} else {
			Debug.Log ("he's naked!");
			dressedBill.SetActive (false);
			undressedBill.SetActive (true);
		}
	}

	public void ChangeToCatch() {
		GameObject undressedBill = GameObject.Find("BillMurray").transform.GetChild(0).gameObject;
		GameObject dressedBill = GameObject.Find("BillMurray").transform.GetChild(1).gameObject;
		GameObject undressedCatch = GameObject.Find("BillMurray").transform.GetChild(2).gameObject;
		GameObject dressedCatch = GameObject.Find("BillMurray").transform.GetChild(3).gameObject;
		if (state.dressed) {
			dressedCatch.SetActive(true);
			dressedBill.SetActive(false);
		} else {
			undressedCatch.SetActive(true);
			undressedBill.SetActive(false);
		}
	}

	void OnTriggerEnter(Collider other) {
		if (other.CompareTag("Baby")) {
			SceneManager.LoadScene("WinScene");
		}
	}
}
