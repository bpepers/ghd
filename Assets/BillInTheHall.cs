﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class BillInTheHall : MonoBehaviour {
	private StateScript state;

	// Use this for initialization
	void Start () {
		state = StateScript.Instance ();

		GameObject undressedBill = GameObject.Find("BillMurray").transform.GetChild(0).gameObject;
		GameObject dressedBill = GameObject.Find("BillMurray").transform.GetChild(1).gameObject;
		if (state.dressed) {
			Debug.Log ("he's dressed");
			dressedBill.SetActive (true);
			undressedBill.SetActive (false);
		} else {
			Debug.Log ("he's naked!");
			dressedBill.SetActive (false);
			undressedBill.SetActive (true);
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (transform.position.y < -10.0f) {
			state.deathText = "falling down an elevator shaft.";
			SceneManager.LoadScene("DeathScene");
		}
	}
}
