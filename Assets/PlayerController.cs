﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

    public float speed = 6.0f;
    public float jumpSpeed = 8.0f;
    public float gravity = 20.0f;

    private Vector3 moveDirection = Vector3.zero;
    private CharacterController controller;
	private Animator animator;

    void Start ()
    {
        controller = GetComponent<CharacterController>();
		animator = GetComponent<Animator>();
    }

    void FixedUpdate ()
    {
		Vector3 oldMove = moveDirection;
		moveDirection = new Vector3(Input.GetAxis("Vertical"), 0, Input.GetAxis("Horizontal"));
		moveDirection *= speed;
		if (Input.GetButton("Jump"))
			moveDirection.y = jumpSpeed;
		if (!controller.isGrounded) {
			oldMove.y -= gravity * Time.deltaTime;
			moveDirection.y = oldMove.y;
		}
		moveDirection.z = -moveDirection.z;
		controller.Move(moveDirection * Time.deltaTime);

		animator.SetFloat ("speed", Mathf.Abs (moveDirection.x) + Mathf.Abs (moveDirection.y) + Mathf.Abs (moveDirection.z));

		UpdateFacing(Input.GetAxis("Vertical"), Input.GetAxis("Horizontal"));
    }

	void UpdateFacing(float x, float z) {
		if (x > 0) {
			transform.rotation = Quaternion.Euler(new Vector3(0, 180, 0));
			return;
		}
		if (x < 0) {
			transform.rotation = Quaternion.Euler(new Vector3(0, 0, 0));
			return;
		}
		if (z > 0) {
			transform.rotation = Quaternion.Euler(new Vector3(0, -90, 0));
			return;
		} else if (z < 0) {
			transform.rotation = Quaternion.Euler(new Vector3(0, 90, 0));
		}
	}
}
