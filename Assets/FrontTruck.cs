﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class FrontTruck : MonoBehaviour {
	bool drive;
	private StateScript state;

	// Use this for initialization
	void Start () {
		drive = false;
		state = StateScript.Instance();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void FixedUpdate() {
		if (drive) {
			Vector3 v = transform.position;
			v.z -= 0.4f;
			transform.position = v;
		}
	}

	public void TriggerTruck() {
		drive = true;
	}

	void OnTriggerEnter(Collider other) {
		state.deathText = "being hit by a car. Who was driving that thing?";
		SceneManager.LoadScene("DeathScene");
	}
}
