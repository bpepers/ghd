﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GetOutOfBed : MonoBehaviour {
	StateScript state;

	void Start() {
		state = StateScript.Instance();
	}

	// Update is called once per frame
	void Update () {
		if (state.leftBed)
			return;
		float horizontal = Input.GetAxis("Horizontal");
		float vertical = Input.GetAxis("Vertical");
		if (horizontal == -1 || vertical == -1) {
			Debug.Log("left");
			if (!state.lightOn) {
				// Need to turn the light on first!
				state.deathText = "falling out of bed in the dark.";
				SceneManager.LoadScene("DeathScene");
			}
			if (!state.satInBed) {
				state.satInBed = true;
				GameObject.Find("BillPjsSitting").GetComponentInChildren<MeshRenderer>().enabled = true;
				GameObject.Find("BillSleeping").SetActive(false);
				state.AddToScore(5);
				GameObject guiObject = GameObject.Find ("GUI");
				if (guiObject) {
					GUIScript guiScript = guiObject.GetComponent<GUIScript>();
					guiScript.DisplayMessage("Just getting up in the morning is a challenge");
				}
				GameObject.Find("bed-made").GetComponentInChildren<MeshRenderer>().enabled = false;
				GameObject.Find("bed-unmade").GetComponentInChildren<MeshRenderer>().enabled = true;
			} else if (state.hitRadio) {
				transform.GetChild(2).gameObject.SetActive(true);
				GameObject.Find("BillPjsSitting").SetActive(false);
				state.leftBed = true;
			}
		} else if (horizontal == 1 && !state.satInBed) {
			// Wrong side of the bed
			state.deathText = "getting up on the wrong side of the bed.";
			SceneManager.LoadScene("DeathScene");
		}
	}
}
