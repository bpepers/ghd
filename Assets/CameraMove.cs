﻿using UnityEngine;
using System.Collections;

public class CameraMove : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		Vector3 moveDirection = new Vector3(0, 0, -Input.GetAxis("Horizontal"));
		moveDirection *= 0.04f;
		Vector3 newPos = transform.position + moveDirection;
		transform.position = newPos;
	}
}
