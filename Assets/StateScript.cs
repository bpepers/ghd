﻿using UnityEngine;
using System.Collections;

public class StateScript : MonoBehaviour {
	private static StateScript instanceRef;

	public bool hitRadio = false;
	public bool satInBed = false;
	public bool lightOn = false;
	public bool dying = false;
	public string deathText = "";
	public int score = 0;
	public int lastDoorOpened = 0;
	public bool dressed = false;
	public bool leftBed = false;
	public bool bedMade = false;
	public bool bellRang = false;

	public AudioClip[] deathSounds;
	public AudioSource audioSource;

	private static GameObject instanceObj;
	public static StateScript Instance() {
		if (instanceObj == null) {
			instanceObj = new GameObject ("State");
			instanceObj.AddComponent<StateScript> ();
		}
		return instanceObj.GetComponent<StateScript> ();
	}

	void Awake() {
		if (instanceRef == null) {
			DontDestroyOnLoad(transform.gameObject);
			instanceRef = this;
		} else {
			DestroyImmediate(transform.gameObject);
		}
	}

	void Start() {
		audioSource = GetComponent<AudioSource>();
	}

	void Update() {
		if (Input.GetKey("escape"))
			Application.Quit();
	}

	void OnLevelWasLoaded() {
		GameObject guiObject = GameObject.Find ("GUI");
		if (guiObject) {
			GUIScript guiScript = guiObject.GetComponent<GUIScript>();
			guiScript.UpdateScore(score);
		}
	}

	public void ResetState() {
		hitRadio = false;
		satInBed = false;
		lightOn = false;
		dying = false;
		lastDoorOpened = 0;
		leftBed = false;
		bedMade = false;
		score = 0;
		bellRang = false;
		dressed = false;
	}

	public float PlayDeathSound() {
		dying = true; 

		if (audioSource.isPlaying)
			return 0.0f;
		audioSource.clip = deathSounds[Random.Range(0, deathSounds.Length)];
		audioSource.Play();
		return audioSource.clip.length;
	}

	public void AddToScore(int points) {
		score += points;
		GameObject guiObject = GameObject.Find ("GUI");
		if (guiObject) {
			GUIScript guiScript = guiObject.GetComponent<GUIScript>();
			guiScript.UpdateScore(score);
		}
	}

	public void SetHighScore() {
		int oldScore = PlayerPrefs.GetInt("highscore", 0);
		if (score > oldScore)
			PlayerPrefs.SetInt("highscore", score);
	}
}
