﻿using UnityEngine;
using System.Collections;

public class DresserScript : MonoBehaviour {
	private StateScript state;

	void Start() {
		state = StateScript.Instance();
	}

	void OnMouseDown() {
		if (state.dressed || !state.leftBed)
			return;
		float d = Vector3.Distance(GameObject.Find("BillWalking").transform.position, transform.parent.position);
		Debug.Log("Dist = " + d);
		if (!state.dressed && d > 1) {
			return;
		}
		GameObject dressedBill = GameObject.Find("BillMurray").transform.GetChild(3).gameObject;
		dressedBill.transform.position = GameObject.Find("BillWalking").transform.position;
		dressedBill.transform.rotation = GameObject.Find("BillWalking").transform.rotation;
		dressedBill.SetActive(true);
		GameObject.Find("BillWalking").SetActive(false);
		state.dressed = true;
		GameObject guiObject = GameObject.Find ("GUI");
		if (guiObject) {
			GUIScript guiScript = guiObject.GetComponent<GUIScript>();
			guiScript.DisplayMessage("I hate putting on pants");
		}
		state.AddToScore(-5);

	}
}
