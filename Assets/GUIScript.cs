﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GUIScript : MonoBehaviour {
	private static GUIScript instanceRef;

	void Awake() {
		if (instanceRef == null) {
			DontDestroyOnLoad(transform.gameObject);
			instanceRef = this;
		} else {
			DestroyImmediate(transform.gameObject);
		}
	}

	public void OnLoadLevel() {
		GetComponent<Canvas>().worldCamera = Camera.main;
	}

	public void UpdateScore(int score) {
		Text scoreText = GameObject.Find("ScoreText").GetComponent<Text>();
		scoreText.text = "Score: " + score;
	}

	public void DisplayMessage(string message) {
		StartCoroutine(ShowMessage(message, 3));
	}

	IEnumerator ShowMessage(string message, float delay) {
		Text messageText = GameObject.Find("MessageText").GetComponent<Text>();
		messageText.text = message;
		yield return new WaitForSeconds(delay);
		messageText.text = "";
	}
}
