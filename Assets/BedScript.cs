﻿using UnityEngine;
using System.Collections;

public class BedScript : MonoBehaviour {
	private StateScript state;

	void Start() {
		state = StateScript.Instance();
	}
	void OnMouseDown() {
		if (!state.leftBed)
			return;
		if (state.bedMade)
			return;
		float d = 2;
		if (!state.dressed) {
			d = Vector3.Distance(GameObject.Find("BillWalking").transform.position, transform.parent.position);
		} else {
			d = Vector3.Distance(GameObject.Find("BillDressedWalking").transform.position, transform.parent.position);
		}
		Debug.Log(d);
		if (d > 2) {
			return;
		}
		state.bedMade = true;
		GameObject guiObject = GameObject.Find ("GUI");
		if (guiObject) {
			GUIScript guiScript = guiObject.GetComponent<GUIScript>();
			guiScript.DisplayMessage("It's best not to get on the maid's bad side");
		}
		state.AddToScore(5);
		GameObject.Find("bed-made").GetComponentInChildren<MeshRenderer>().enabled = true;
		GameObject.Find("bed-unmade").GetComponentInChildren<MeshRenderer>().enabled = false;
	}
}
