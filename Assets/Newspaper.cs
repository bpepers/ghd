﻿using UnityEngine;
using System.Collections;

public class Newspaper : MonoBehaviour {
	private StateScript state;

	void Start() {
		state = StateScript.Instance ();
	}

	void OnMouseDown() {
		float d = Vector3.Distance(GameObject.Find("BillMurray").transform.position, transform.position);
		if (d > 1.8f) {
			return;
		}

		state.AddToScore (5);

		GameObject guiObject = GameObject.Find ("GUI");
		if (guiObject) {
			GUIScript guiScript = guiObject.GetComponent<GUIScript>();
			guiScript.DisplayMessage("Avoid freak lightning by hesitating on doorsteps. Huh.");
		}
	}
}
