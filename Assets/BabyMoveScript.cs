﻿using UnityEngine;
using System.Collections;

public class BabyMoveScript : MonoBehaviour {
	private bool moving = false;

	// Use this for initialization
	void Start () {
		Invoke ("StartMoving", 5);
	}

	void StartMoving() {
		moving = true;
	}

	// Update is called once per frame
	void FixedUpdate () {
		if (moving) {
			Vector3 v = transform.position;
			v.x -= 0.01f;
			if (v.x < -6.7f)
				v.y = 0;
			transform.position = v;
		}	
	}
}
