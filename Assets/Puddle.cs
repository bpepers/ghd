﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Puddle : MonoBehaviour {
	private StateScript state;

	void Start() {
		state = StateScript.Instance();
	}

	void OnTriggerEnter(Collider other) {
		state.deathText = "drowning in a very deep puddle";
		SceneManager.LoadScene("DeathScene");
	}
}
