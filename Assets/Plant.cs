﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Plant : MonoBehaviour {
	private StateScript state;

	void Start() {
		state = StateScript.Instance();
	}

	void OnMouseDown() {
		float d = Vector3.Distance(GameObject.Find("BillMurray").transform.position, transform.parent.position);
		Debug.Log("Dist = " + d);
		if (d > 1.2) {
			return;
		}
		state.deathText = "a decorative cactus";
		SceneManager.LoadScene("DeathScene");
	}
}
