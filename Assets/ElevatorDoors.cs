﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ElevatorDoors : MonoBehaviour {
	private StateScript state;

	void Start() {
		state = StateScript.Instance();
	}

	void OnTriggerEnter(Collider other) {
		state.deathText = "being crushed between the elevator doors";
		SceneManager.LoadScene("DeathScene");
	}
}
