﻿using UnityEngine;
using System.Collections;

public class BellboyScript : MonoBehaviour {
	private float finalY = 0.03f;
	private bool rising = false;

	void FixedUpdate() {
		if (rising && transform.parent.position.y >= finalY) {
			rising = false;
			Vector3 v = transform.parent.position;
			v.y = finalY;
			transform.parent.position = v;
	
			GameObject guiObject = GameObject.Find ("GUI");
			if (guiObject) {
				GUIScript guiScript = guiObject.GetComponent<GUIScript>();
				guiScript.DisplayMessage("Let me unlock that door for you!");
			}
			return;
		}
		if (rising) {
			Vector3 v = transform.parent.position;
			v.y += 0.01f;
			transform.parent.position = v;
		}
	}

	public void StartRise() {
		rising = true;
	}
}
