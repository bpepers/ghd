﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Baby : MonoBehaviour {
	bool flinging;
	int flingCounter;
	private StateScript state;

	// Use this for initialization
	void Start () {
		flinging = false;
		flingCounter = 0;
		Invoke("FlingBaby", 10);
		state = StateScript.Instance();
	}

	void FlingBaby() {
		Debug.Log("Fling!");
		flinging = true;
	}

	void FixedUpdate() {
		if (flinging) {
			float thrust = 2;
			GetComponent<Rigidbody>().AddForce(-transform.right * thrust);
			if (flingCounter++ > 2) {
				flinging = false;
			}
		}
	}

	// Update is called once per frame
	void Update () {
		if (transform.position.y < 1.5) {
			state.deathText = "shame at failing to save the baby.";
			SceneManager.LoadScene("DeathScene");
		}
	}
}
