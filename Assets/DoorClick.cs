﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class DoorClick : MonoBehaviour {
	public int doorNumber;

	private StateScript state;

	// Use this for initialization
	void Start () {
		state = StateScript.Instance();
	}

	// clicked on a door
	void OnMouseDown() {
		float d = Vector3.Distance(GameObject.Find("BillMurray").transform.position, transform.parent.position);
		Debug.Log("Dist = " + d);
		if (d > 1.8f) {
			return;
		}

		AudioSource music = GetComponent<AudioSource> ();
		music.Play ();
		Invoke ("AfterPlaying", 1);
	}

	void AfterPlaying() {
		GameObject guiObject = GameObject.Find ("GUI");

		switch (state.lastDoorOpened) {
		case 0:
			if (doorNumber != 9) {
				state.deathText = "a bear attack.  Who'd have guessed!";
				SceneManager.LoadScene ("DeathScene");
			} else if (guiObject) {
				GUIScript guiScript = guiObject.GetComponent<GUIScript>();
				guiScript.DisplayMessage("You have a lovely chat!");
			}
			break;
		case 9:
			if (doorNumber != 11) {
				state.deathText = "nothing discernable at all.";
				SceneManager.LoadScene ("DeathScene");
			} else if (guiObject) {
				GUIScript guiScript = guiObject.GetComponent<GUIScript>();
				guiScript.DisplayMessage("Yet another lovely person talked to!");
			}
			break;
		case 11:
			if (doorNumber != 10) {
				state.deathText = "a ninja slicing you in half.";
				SceneManager.LoadScene ("DeathScene");
			} else if (guiObject) {
				GUIScript guiScript = guiObject.GetComponent<GUIScript>();
				guiScript.DisplayMessage("What person was weird.  But good weird!");
			}
			break;
		case 10:
			if (doorNumber != 12) {
				state.deathText = "a heart attack.  What did you see??!!";
				SceneManager.LoadScene ("DeathScene");
			} else if (guiObject) {
				GUIScript guiScript = guiObject.GetComponent<GUIScript>();
				guiScript.DisplayMessage("Well it's sure been nice talking to all these people!");
			}
			break;
		case 12:
			state.deathText = "opening too many doors!!";
			SceneManager.LoadScene ("DeathScene");
			break;
		}

		state.lastDoorOpened = doorNumber;
		state.AddToScore (5);
	}
}
