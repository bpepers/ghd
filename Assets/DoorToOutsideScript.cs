﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class DoorToOutsideScript : MonoBehaviour {
	private StateScript state;

	void Start() {
		state = StateScript.Instance();
	}

	void OnTriggerEnter(Collider other) {
		if (!state.bellRang) {
			GameObject guiObject = GameObject.Find ("GUI");
			if (guiObject) {
				GUIScript guiScript = guiObject.GetComponent<GUIScript>();
				guiScript.DisplayMessage("The door is locked.");
			}
			return;
		}
		SceneManager.LoadScene("OutsideScene");
	}
}
