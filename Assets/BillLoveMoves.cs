﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class BillLoveMoves : MonoBehaviour {
	private StateScript state;
	private Animator animator;
	private bool moving = false;
	private float finalX = -0.1f;

	// Use this for initialization
	void Start () {
		state = StateScript.Instance ();

		GameObject guiObject = GameObject.Find ("GUI");
		if (guiObject) {
			GUIScript guiScript = guiObject.GetComponent<GUIScript>();
			guiScript.DisplayMessage("Maximum score was 50.");
		}

		GameObject undressedBill = GameObject.Find("BillMurray").transform.GetChild(0).gameObject;
		GameObject dressedBill = GameObject.Find("BillMurray").transform.GetChild(1).gameObject;
		if (state.dressed) {
			Debug.Log ("he's dressed");
			dressedBill.SetActive (true);
			undressedBill.SetActive (false);
			animator = dressedBill.GetComponent<Animator> ();
		} else {
			Debug.Log ("he's naked!");
			dressedBill.SetActive (false);
			undressedBill.SetActive (true);
			animator = undressedBill.GetComponent<Animator> ();
		}

		Invoke ("StartMoving", 2);
		Invoke("CheckClothes", 3);
	}

	void StartMoving() {
		moving = true;
		animator.SetFloat ("speed", 1.0f);
	}

	void CheckClothes() {
		if (!state.dressed) {
			GameObject guiObject = GameObject.Find ("GUI");
			if (guiObject) {
				GUIScript guiScript = guiObject.GetComponent<GUIScript>();
				guiScript.DisplayMessage("Wait a minute, are you not dressed? Ew.");
			}
			GameObject.Find("MotherInLove").GetComponent<AudioSource>().Stop();
			GameObject.Find("MotherInLove").GetComponent<WomanLoveMoves>().RunAway();
			moving = false;
			Invoke("DieOfShame", 2);
		}
	}

	void DieOfShame() {
		state.deathText = "a broken heart. Dress better next time.";
		SceneManager.LoadScene("DeathScene");
	}

	// Update is called once per frame
	void FixedUpdate () {
		if (moving && transform.position.x >= finalX) {
			moving = false;
			Vector3 v = transform.position;
			v.x = finalX;
			transform.position = v;
			animator.SetFloat ("speed", 0.0f);
		} else if (moving) {
			Vector3 v = transform.position;
			if (v.x >= -1.2f && v.x <= -1.0f)
				v.x += 0.001f;
			else
				v.x += 0.1f;
			transform.position = v;
		}
	}
}
