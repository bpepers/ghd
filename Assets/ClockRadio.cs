using UnityEngine;
using System.Collections;

public class ClockRadio : MonoBehaviour {
	public Camera mainCamera;
	private StateScript state;

	private bool switched;

	// Use this for initialization
	void Start () {
		state = StateScript.Instance();

		switched = false;

		// move the camera to be looking at the clock
		mainCamera.transform.position = new Vector3 (-1.61f, .51f, 1.63f);
		mainCamera.transform.rotation = Quaternion.Euler(0.0f, 0.0f, 0.0f);
		mainCamera.orthographicSize = .2f;

		// make sure clock is showing 5:59
		transform.GetChild (0).gameObject.SetActive (true);
		transform.GetChild (1).gameObject.SetActive (false);
		transform.GetChild(2).gameObject.SetActive(false);
		transform.GetChild(3).gameObject.SetActive(false);

		Invoke("ClockAt600", 1);
	}
	
	// Switch the clock to the 6:00 one
	void ClockAt600() {
		// switch display to 6:00
		transform.GetChild (0).gameObject.SetActive (false);
		transform.GetChild (1).gameObject.SetActive (true);

		// start the music
		AudioSource music = GetComponent<AudioSource> ();
		music.Play ();

		Invoke("ClockAt601", 2);
	}

	// Switch the clock to the 6:01 one
	void ClockAt601() {
		// switch display to 6:01
		transform.GetChild (1).gameObject.SetActive (false);
		transform.GetChild (2).gameObject.SetActive (true);

		Invoke("ClockAt602", 2);
	}

	void ClockAt602() {
		transform.GetChild(2).gameObject.SetActive(false);
		transform.GetChild(3).gameObject.SetActive(true);

		Invoke("SwitchView", 2);
	}

	// Switch the view to the normal room
	void SwitchView() {
		// restore normal camera angle and position
		mainCamera.transform.position = new Vector3 (-2.8f, 2.15f, -3.0f);
		mainCamera.transform.rotation = Quaternion.Euler(20.0f, 45.0f, 0.0f);
		mainCamera.orthographicSize = 3.0f;

		switched = true;
	}

	// Update is called once per frame
	void Update () {	
	}

	// turn on/off when clicked
	void OnMouseDown() {
		if (switched) {
			if (!state.hitRadio) {
				state.AddToScore(10);
				state.hitRadio = true;
				GameObject guiObject = GameObject.Find ("GUI");
				if (guiObject) {
					GUIScript guiScript = guiObject.GetComponent<GUIScript>();
					guiScript.DisplayMessage("If I listen to that song for one more second I'll lose my mind");
				}
			}
			AudioSource music = GetComponent<AudioSource> ();
			music.volume = 1.0f - music.volume;
		}
	}
}
