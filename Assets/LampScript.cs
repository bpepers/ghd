﻿using UnityEngine;
using System.Collections;

public class LampScript : MonoBehaviour {
	void OnMouseDown() {
		float d = Vector3.Distance(GameObject.Find("BillMurray").transform.position, transform.parent.position);
		Debug.Log("Dist = " + d);
		if (d > 1.7) {
			return;
		}
		foreach (Light l in transform.parent.gameObject.GetComponentsInChildren<Light>()) {
			l.enabled = !l.enabled;
		}
	}
}
