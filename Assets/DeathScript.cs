﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class DeathScript : MonoBehaviour {
	public AudioClip[] deathSounds;
	public AudioSource audioSource;

	private StateScript stateScript;

	void Start() {
		stateScript = StateScript.Instance ();

		Text deathText = GameObject.Find("DeathText").GetComponent<Text>();
		deathText.text += stateScript.deathText;

		audioSource = GetComponent<AudioSource>();
		float clipLength = PlayDeathSound();
		Invoke("RestartLevel", clipLength + 1);

		stateScript.ResetState();
	}

	void RestartLevel() {
		Debug.Log("Restart level");
		//Application.LoadLevel(0);
		SceneManager.LoadScene("TestScene");
	}

	public float PlayDeathSound() {
		if (audioSource.isPlaying)
			return 0.0f;
		audioSource.clip = deathSounds[Random.Range(0, deathSounds.Length)];
		audioSource.Play();
		return audioSource.clip.length;
	}
}
