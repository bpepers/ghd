﻿using UnityEngine;
using System.Collections;

public class ElevatorSwitch : MonoBehaviour {
	private StateScript state;

	void Start() {
		state = StateScript.Instance ();
	}

	void OnMouseDown() {
		float d = Vector3.Distance (GameObject.Find ("BillMurray").transform.position, transform.position);
		if (d > 1.8f) {
			return;
		}

		ElevatorControl script = GameObject.Find ("Elevator").GetComponent<ElevatorControl> ();
		script.CallElevator ();

		transform.GetChild (0).gameObject.GetComponent<MeshRenderer> ().materials [0].color = Color.red;

		if (state.lastDoorOpened == 0) {
			state.AddToScore (-10);
			GameObject guiObject = GameObject.Find ("GUI");
			if (guiObject) {
				GUIScript guiScript = guiObject.GetComponent<GUIScript> ();
				guiScript.DisplayMessage ("You didn't talk to anyone.  Not very friendly...");
			}
		}
	}
}
