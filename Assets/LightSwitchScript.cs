﻿using UnityEngine;
using System.Collections;

public class LightSwitchScript : MonoBehaviour {
	public Light controlledLight;
	private StateScript state;

	void Start() {
		state = StateScript.Instance();
	}

	void OnMouseDown() {
		controlledLight.enabled = !controlledLight.enabled;
		state.lightOn = controlledLight.enabled;
		transform.Rotate(180, 0, 0);
		transform.Translate(0, -0.4f, 0);
		GameObject guiObject = GameObject.Find ("GUI");
		if (guiObject) {
			GUIScript guiScript = guiObject.GetComponent<GUIScript>();
			guiScript.DisplayMessage("Click!");
		}
	}
}
